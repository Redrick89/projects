import Vue from 'vue'
import App from './App.vue'
import Basket from './Basket.vue'

new Vue({
  el: '#app',
  render: h => h(App)
})
